# README #

TPFormulaire est une librairie permettant de faciliter la création de questionnaires.

Auteurs : Jonathan Salamon et Damien Sueur

### Fonctionnalités ###
* Possibilité de créer ses propres types de questions et de réponses
* Compatible WinForm
* Gestion de plugins
* Sauvegarde de formulaire et des réponses (dans des fichiers séparés)
* Possibilité de charger des réponses ou formulaires existant + modification
* Les questions peuvent appartenir à un groupe

### Installation ###
Cloner ce repository et ouvrez TPFormulaire.csproj.

Générer le plugin d'exemple TPFormulaire.MyPlugin.

TPFormulaire.MyPlugin.dll doit être ajouté à un dossier "templates" au sein du dossier où est généré votre application (Par défaut : VotreProjet/bin/debug).

Tout plugin ajouté au dossier templates sera automatiquement chargé par la librairie.

Pour utiliser la librairie, créez un nouveau Form :

                TPFormulaire.Main.Form f = new TPFormulaire.Main.Form();

Vous pouvez utiliser toutes les méthodes et propriétés de cet objet.
Il devrait suffir pour effectuer toutes les actions possibles avec notre librairie.
Une documentation est disponible dans le code source.

### Lancer le projet d'exemple ###
Un dossier "Examples" est disponible.
Lancez simplement l'application TPFormulaire.
Une fois l'application ouverte, vous pouvez charger un formulaire d'exemple (qui se trouve dans ce même dossier).
Pour cela, cliquez sur "Charger un formulaire existant" et choisissez "HG67-Bis.bin".
Pour charger des réponses, cliquez sur "Voir les réponses", puis "Charger les réponses" et sélectionnez "HG67-Bis-Answers.bin".

### Tests Unitaires ###
D'autres exemples et cas d'utilisation sont disponibles au sein du dossier "Unit Tests".
Lancez nunit et charger la dll "TPFormulaire.Tests.dll" pour consulter et lancer les tests unitaires.

### Créer son type de question/réponse ###
Vous pouvez vous inspirer de OpenQuestion.cs, OpenAnswer.cs et LinkQuestion.cs pour créer vos types de questions et de réponses.
Votre classe doit hériter de OpenQuestion ou OpenAnswer selon ce que vous voulez créer.

En ce qui concerne l'affichage, il est défini par défaut, mais vous pouvez redéfinir les méthodes GetControl() (affichage de la question ou du champ de réponse) et/ou GetCreateControl() (affichage du champ permettant de créer la question).
Vous pouvez retourner n'importe quel Control afin de personnaliser l'affichage de vos types de questions et types de réponses.

Plus d'informations sont disponibles au sein des fichiers TPFormulaire.Main>models>QuestionType.cs et TPFormulaire.Main>models>AnswerType.cs