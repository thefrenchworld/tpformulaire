﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TPFormulaire.Main;

namespace TPFormulaire.Tests
{
    [TestFixture]
    public class FormTests
    {
        [Test]
        public void CreateAnswers()
        {
            Form f = new Form();


            LinkQuestion lQuestion = new LinkQuestion("http://www.google.fr");
            f.CreateQuestion(lQuestion, "Question 1");
            f.CreateQuestion("TPFormulaire.Main.OpenQuestion", "Question 2");
            f.CreateQuestion("TPFormulaire.Main.OpenQuestion", "Question 3");



            Assert.IsNull(f.Title);
            f.Title = "jj";
            Assert.AreEqual("jj", f.Title);

            Answer a = f.FindOrCreateAnswer("TPFormulaire.Main.OpenAnswer", "Emilie");
            Assert.IsNotNull(a);

            Answer b = f.FindOrCreateAnswer("TPFormulaire.Main.OpenAnswer","Emilie");
            Assert.AreSame(a, b);

            Assert.AreEqual(1, f.AnswerCount);
            Answer c = f.FindOrCreateAnswer("TPFormulaire.Main.OpenAnswer", "John Rambo");
            Assert.AreNotSame(a, c);

            Assert.AreEqual("Emilie", (String)a.GetAnswer());
            Assert.AreEqual("John Rambo", (String)c.GetAnswer());
        }

        [Test]
        public void CreateQuestionFolders()
        {
            Form f = new Form();
            f.Title = "HG67-Bis";
            Assert.AreEqual("HG67-Bis", f.Title);
            Question q1 = f.CreateQuestion("TPFormulaire.Main.OpenQuestion", "Q1");
            Question q2 = f.CreateQuestion("TPFormulaire.Main.OpenQuestion", "Q2");
            Question q3 = f.CreateQuestion("TPFormulaire.Main.OpenQuestion", "Q3", "Groupe 2");
            Assert.AreEqual(0, q1.Index);
            Assert.AreEqual(1, q2.Index);
            Assert.AreEqual(2, q3.Index);
            q2.Index = 0;
            Assert.AreEqual(0, q2.Index);
            Assert.AreEqual(1, q1.Index);
            Assert.AreEqual(2, q3.Index);

            QuestionGroup group = f.CreateOrFindQuestionGroup("Groupe 1");
            f.AddToGroup(group, q2);
            Assert.IsTrue(q2.Group == group);

            QuestionGroup group2 = f.CreateOrFindQuestionGroup("Groupe 2");
            Assert.IsTrue(q3.Group == group2);
        }

        [Test]
        public void RemoveQuestionFromGroup()
        {
            Form f = new Form();
            f.Title = "HG67-Bis";
            Assert.AreEqual("HG67-Bis", f.Title);
            Question q1 = f.CreateQuestion("TPFormulaire.Main.OpenQuestion", "Q1");
            Question q2 = f.CreateQuestion("TPFormulaire.Main.OpenQuestion", "Q2");
            Assert.AreEqual(0, q1.Index);
            Assert.AreEqual(1, q2.Index);
            q2.Index = 0;
            Assert.AreEqual(0, q2.Index);
            Assert.AreEqual(1, q1.Index);

            QuestionGroup group = f.CreateOrFindQuestionGroup("Groupe 1");
            f.AddToGroup(group, q2);
            Assert.IsTrue(q2.Group == group);

            f.RemoveFromGroup(group, q2);
            Assert.IsTrue(q2.Group != group);
        }

        [Test]
        public void AddAnswerForQuestion()
        {
            Form f = new Form();
            f.Title = "HG67-Bis";
            Question q1 = f.CreateQuestion("TPFormulaire.Main.OpenQuestion", "Q1");
            Question q2 = f.CreateQuestion("TPFormulaire.Main.OpenQuestion", "Q2");
            Answer a = f.FindOrCreateAnswer("TPFormulaire.Main.OpenAnswer", "Emilie");
            Answer b = f.FindOrCreateAnswer("TPFormulaire.Main.OpenAnswer", "John Rambo");

            FormAnswer fa = new FormAnswer();
            fa.AddAnswerFor(q1, a);
            fa.AddAnswerFor(q2, b);

            Assert.IsTrue(fa.Answers.Count == 2);
            KeyValuePair<Question, Answer> pair = new KeyValuePair<Question, Answer>(q1, a);
            KeyValuePair<Question, Answer> pair2 = new KeyValuePair<Question, Answer>(q2, b);
            Assert.IsTrue(fa.Answers.Contains(pair));
            Assert.IsTrue(fa.Answers.Contains(pair2));
        }

        [Test]
        public void FindAnswerFromQuestion()
        {
            Form f = new Form();
            f.Title = "HG67-Bis";
            Question q1 = f.CreateQuestion("TPFormulaire.Main.OpenQuestion", "Q1");
            Question q2 = f.CreateQuestion("TPFormulaire.Main.OpenQuestion", "Q2");
            Answer a = f.FindOrCreateAnswer("TPFormulaire.Main.OpenAnswer", "Emilie");
            Answer b = f.FindOrCreateAnswer("TPFormulaire.Main.OpenAnswer", "John Rambo");

            FormAnswer fa = new FormAnswer();
            fa.AddAnswerFor(q1, a);
            fa.AddAnswerFor(q2, b);

            Assert.IsTrue(fa.Answers.Count == 2);
            KeyValuePair<Question, Answer> pair = new KeyValuePair<Question, Answer>(q1, a);
            KeyValuePair<Question, Answer> pair2 = new KeyValuePair<Question, Answer>(q2, b);

            Answer c = fa.FindAnswer(q1);
            Answer d = fa.FindAnswer(q2);

            Assert.AreEqual(a, c);
            Assert.AreEqual(b, d);
        }

        [Test]
        public void BackupFormToFile()
        {
            Form f = new Form();
            f.Title = "HG67-Bis";
            LinkQuestion lQuestion = new LinkQuestion("http://www.google.fr");
            Question q1 = f.CreateQuestion(lQuestion, "Q1");
            Question q2 = f.CreateQuestion("TPFormulaire.Main.OpenQuestion", "Q2");
            Answer a = f.FindOrCreateAnswer("TPFormulaire.Main.OpenAnswer", "Emilie");
            Answer b = f.FindOrCreateAnswer("TPFormulaire.Main.OpenAnswer", "John Rambo");

            f.SaveForm();

            Form form = f.LoadForm(f.Title+".bin");

            Assert.AreEqual(f.Title, form.Title);
        }

        [Test]
        public void BackupAnswersToFile()
        {
            Form f = new Form();
            f.Title = "HG67-Bis";
            Question q1 = f.CreateQuestion("TPFormulaire.Main.OpenQuestion", "Q1");
            Question q2 = f.CreateQuestion("TPFormulaire.Main.OpenQuestion", "Q2");
            Answer a = f.FindOrCreateAnswer("TPFormulaire.Main.OpenAnswer", "Emilie");
            Answer b = f.FindOrCreateAnswer("TPFormulaire.Main.OpenAnswer", "John Rambo");


            f.SaveAnswers(f.Title+"_answers");

            Form formA = new Form();
            formA.Title = "A724EC";
            formA.LoadAnswers(f.Title + "_answers.bin");
            
            Assert.IsTrue(f.Answers.Count == formA.Answers.Count);
        }

    }
}
