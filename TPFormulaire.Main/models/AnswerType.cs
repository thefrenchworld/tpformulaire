﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TPFormulaire.Main
{
    /// <summary>
    /// Extend this class in order to create a new type of Answer
    /// Please don't forget to add Serializable attribute
    /// </summary>
    [Serializable]
    public abstract class AnswerType
    {
        private object _answer;
        public AnswerType()
        {
        }
        /// <summary>
        /// Add an answer object
        /// </summary>
        /// <param name="answer"></param>
        public virtual void AddAnswer(object answer) {
            _answer = answer;
        }
        /// <summary>
        /// Return answer object
        /// </summary>
        /// <returns></returns>
        abstract public object GetAnswer();

        /// <summary>
        /// Get Display Control for this type of Answer
        /// Default : shows a Textbox containing answer
        /// A listener is called each time text changed in Textbox
        /// This listener calls AddAnswer
        /// </summary>
        /// <returns></returns>
        public virtual Control GetControl()
        {
            TextBox textbox = new TextBox();
            textbox.Text = (string)_answer;

            textbox.TextChanged += Answered;
            return textbox;
        }

        void Answered(object sender, EventArgs e)
        {
            AddAnswer(((TextBox) sender).Text);
        }
    }
}
