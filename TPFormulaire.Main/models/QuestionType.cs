﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TPFormulaire.Main
{
    /// <summary>
    /// Extend this class in order to create a new type of Question
    /// Please don't forget to add Serializable attribute
    /// </summary>
    [Serializable]
    public abstract class QuestionType
    {
        private object _questionTitle;

        public virtual void SetQuestionTitle(object questionTitle)
        {
            _questionTitle = questionTitle;
        }

        /// <summary>
        /// Return object reprensenting the title of this question.
        /// This can be any object
        /// </summary>
        /// <returns></returns>
        public virtual object GetQuestionTitle()
        {
            return _questionTitle;
        }

        /// <summary>
        /// Returns a list of accepted AnswersType for this type of question
        /// </summary>
        /// <returns></returns>
        public abstract List<AnswerType> GetAcceptedAnswerTypes();

        /// <summary>
        /// Can answer be empty?
        /// </summary>
        /// <returns></returns>
        public virtual bool AcceptEmptyAnswers()
        {
            return true;
        }

        /// <summary>
        /// Clone this question type (needed for serialization)
        /// </summary>
        /// <returns></returns>
        public abstract QuestionType clone();

        /// <summary>
        /// Get Display Control for this type of Question
        /// Default : shows a label containing questionTitle
        /// </summary>
        /// <returns></returns>
        public virtual Control GetControl()
        {
            Label label = new Label();
            label.Text = (string) _questionTitle;
            return label;
        }

        /// <summary>
        /// Get a control used to build this question type
        /// Default : shows a Textbox
        /// </summary>
        /// <returns></returns>
        public virtual Control GetCreateControl()
        {
            TextBox textbox = new TextBox();
            return textbox;
        }

    }
}
