﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;

namespace TPFormulaire.Main
{
    [Serializable]
    public class Activator
    {
        List<Type> _templates;
        String[] _moduleDirectories = new String[] { "templates" };
        public Activator()
        {
            _templates = new List<Type>();

            foreach (String filename in System.IO.Directory.GetFiles(System.IO.Directory.GetCurrentDirectory() + "/templates"))
            {
                if (System.IO.Path.GetExtension(filename) == ".dll")
                {
                    Assembly dll = Assembly.LoadFrom(filename);
                    Type[] types = dll.GetExportedTypes();
                    for (int i = 0; i < types.Length; i++)
                    {
                        Type type = types[i];
                        if (type == null)
                        {
                            continue;
                        }
                        if (type.BaseType.FullName.Equals("TPFormulaire.Main.AnswerType") || type.BaseType.FullName.Equals("TPFormulaire.Main.QuestionType"))
                        {
                            _templates.Add(type);
                        }
                    }
                }
            }

        }

        /**
         * Load Plugins. Registrer an event to be called at each AppDomain.CurrentDomain.Load()
         * Our event will be called and we will search for dll into base directory and moduleDirectories in order to find plugins
         * */
        public void LoadPlugins()
        {
            AppDomain.CurrentDomain.AssemblyResolve += CurrentDomain_AssemblyResolve;
        }

        private Assembly CurrentDomain_AssemblyResolve(object sender, ResolveEventArgs args)
        {
            //Load from initial dir
            var currentDir = new DirectoryInfo(System.IO.Directory.GetCurrentDirectory());
            var root = currentDir.GetFiles().FirstOrDefault(i => i.Name == args.Name + ".dll");
            if (root != null)
            {
                return Assembly.LoadFrom(root.FullName);
            }

            //Load from plugins directories
            foreach (var moduleDir in _moduleDirectories)
            {
                var dir = new DirectoryInfo(System.IO.Directory.GetCurrentDirectory() + "\\" + moduleDir);
                var module = dir.GetFiles().FirstOrDefault(i => i.Name == args.Name + ".dll");
                if (module != null)
                {
                    return Assembly.LoadFrom(module.FullName);
                }
            }
            //Not found
            return null;
        }

        public List<Type> GetAvailableTemplates()
        {
            return _templates;
        }

        public QuestionType getQuestionType(String name)
        {
            foreach (Type type in _templates)
            {
                if (type.FullName.Equals(name))
                {
                    return System.Activator.CreateInstance(type) as QuestionType;
                }
            }
            return null;
        }

        public AnswerType getAnswerType(String name)
        {
            foreach (Type type in _templates)
            {
                if (type.FullName.Equals(name))
                {
                    return System.Activator.CreateInstance(type) as AnswerType;
                }
            }
            return null;
        }

        
    }
}
