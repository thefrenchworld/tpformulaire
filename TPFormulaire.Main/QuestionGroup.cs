﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TPFormulaire.Main
{
    [Serializable]
    public class QuestionGroup
    {
        private string _groupUniqueName;

        public string GroupUniqueName
        {
            get { return _groupUniqueName; }
            set { _groupUniqueName = value; }
        }

        private QuestionGroup()
        {

        }

        internal QuestionGroup(string groupUniqueName)
        {
            this._groupUniqueName = groupUniqueName;
        }

        public QuestionGroup clone()
        {
            return new QuestionGroup(_groupUniqueName);
        }
    }
}
