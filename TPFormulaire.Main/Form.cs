﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace TPFormulaire.Main
{
    /// <summary>
    /// Main Class to use in order to create and interact with a form
    /// All publics methods are listed into this single class
    /// </summary>
    [Serializable]
    public class Form
    {
        private string _title;
        private List<QuestionGroup> _questionGroups;
        private List<Question> _questions;
        [NonSerialized]
        private FormAnswer _formAnswer;
        [NonSerialized]
        private Activator _activator;

        public IReadOnlyDictionary<Question, Answer> Answers
        {
            get { return _formAnswer.Answers; }
        }

        public string Title
        {
            get { return _title; }
            set { _title = value; }
        }

        public IReadOnlyList<Question> Questions
        {
            get { return _questions; }
        }

        public IReadOnlyList<QuestionGroup> QuestionGroups
        {
            get { return _questionGroups; }
        }

        public int AnswerCount
        {
            get { return _formAnswer.CountAnswers(); }
        }

        public IReadOnlyList<Type> Types
        {
            get { return _activator.GetAvailableTemplates(); }
        }

        public Form()
        {
            _questionGroups = new List<QuestionGroup>();
            _questions = new List<Question>();
            _formAnswer = new FormAnswer();
            _activator = new Activator();
        }
        /// <summary>
        /// Create a new question.
        /// You can indicate any type provided by plugins added to your project
        /// Example : TPFormulaire.Main.OpenQuestion
        /// </summary>
        /// <param name="type"></param>
        /// <param name="title"></param>
        /// <returns></returns>
        public Question CreateQuestion(string type, object title)
        {
            if (type == null || type.Length == 0)
            {
                throw new ArgumentNullException("type can't be null or empty");
            }
            if (title == null)
            {
                throw new ArgumentNullException("title can't be null or empty");
            }

            if (_activator.getQuestionType(type) == null)
            {
                throw new DllNotFoundException("Can't find type : " + type + ". Is Template missing?");
            }


            return CreateQuestion(_activator.getQuestionType(type), title);
        }

        /// <summary>
        /// Same as CreateQuestion(string type, string title) but with a give QuestionType
        /// </summary>
        /// <param name="type"></param>
        /// <param name="title"></param>
        /// <returns></returns>
        public Question CreateQuestion(QuestionType type, object title)
        {
            if (type == null)
            {
                throw new ArgumentNullException("type can't be null");
            }

            Question question = new Question(title, type, null, this);
            _questions.Add(question);

            return question;
        }
        /// <summary>
        /// Same as CreateQuestion(type, title) but you can specify a group
        /// Question will be added to existing group or a new group will be created
        /// </summary>
        /// <param name="type"></param>
        /// <param name="title"></param>
        /// <param name="group"></param>
        /// <returns></returns>
        public Question CreateQuestion(string type, object title, string group)
        {
            if (type == null || type.Length == 0)
            {
                throw new ArgumentNullException("type can't be null or empty");
            }
            if (title == null)
            {
                throw new ArgumentNullException("title can't be null or empty");
            }
            if (group == null || group.Length == 0)
            {
                throw new ArgumentNullException("group can't be null or empty");
            }

            if (_activator.getQuestionType(type) == null)
            {
                throw new DllNotFoundException("Can't find type : " + type + ". Is Template missing?");
            }

            foreach (QuestionGroup questionGroup in _questionGroups)
            {
                if (questionGroup.GroupUniqueName.Equals(group))
                {
                    Question question = new Question(title, _activator.getQuestionType(type), questionGroup, this);
                    _questions.Add(question);
                    return question;
                }
            }

            //Create group and add it, then add question
            QuestionGroup newGroup = new QuestionGroup(group);
            _questionGroups.Add(newGroup);
            Question newQuestion = new Question(title, _activator.getQuestionType(type), newGroup, this);
            _questions.Add(newQuestion);

            return newQuestion;
        }
        /// <summary>
        /// Find a Group by name, Create it if no group exists with this name
        /// </summary>
        /// <param name="uniqueName"></param>
        /// <returns></returns>
        public QuestionGroup CreateOrFindQuestionGroup(string uniqueName)
        {
            if (uniqueName == null || uniqueName.Length == 0)
            {
                throw new ArgumentNullException("uniqueName can't be null or empty");
            }

            foreach (Question question in _questions)
            {
                if (question.Group != null && question.Group.GroupUniqueName.Equals(uniqueName))
                {
                    return question.Group;
                }
            }

            return new QuestionGroup(uniqueName);
        }
        /// <summary>
        /// Add a question to a Group
        /// </summary>
        /// <param name="group"></param>
        /// <param name="question"></param>
        /// <returns></returns>
        public QuestionGroup AddToGroup(QuestionGroup group, Question question)
        {
            if (question.Group == null)
            {
                question.Group = group;
            }
            else
            {
                throw new InvalidOperationException("This question is already part of group : " + question.Group.GroupUniqueName + " . Question can be part of only ONE group");
            }
            return question.Group;
        }
        /// <summary>
        /// Remove a question from a group
        /// </summary>
        /// <param name="group"></param>
        /// <param name="question"></param>
        public void RemoveFromGroup(QuestionGroup group, Question question)
        {
            if (group == question.Group)
            {
                question.Group = null;
            }
        }
        /// <summary>
        /// Find Existing answer or create new one.
        /// Each time you call this method providing a new answer, this answer will be automatically added to the first question without answer yet
        /// Example : First Call -> create a new Answer for Question 1
        /// Second Call -> Create a new Answer for Question 2 (if this answer is not the same as Question 1's)
        /// </summary>
        /// <param name="type"></param>
        /// <param name="answerObject"></param>
        /// <returns></returns>
        public Answer FindOrCreateAnswer(String type, object answerObject)
        {
            if (_activator.getAnswerType(type) == null)
            {
                throw new DllNotFoundException("Can't find type : " + type + ". Is Template missing?");
            }
            Answer answer = new Answer(_activator.getAnswerType(type), answerObject);
            foreach (Question question in _questions)
            {
                Answer savedAnswer = _formAnswer.FindAnswer(question);
                if (savedAnswer != null && savedAnswer.GetType() == answer.GetType() && savedAnswer.GetAnswer() == answer.GetAnswer())
                {
                    return savedAnswer;
                }
            }

            return CreateAnswer(type, answerObject);
        }

        /// <summary>
        /// Same as FindOrCreateAnswer(String type, object answerObject)
        /// but you can directly specify your own AnswerType object
        /// </summary>
        /// <param name="type"></param>
        /// <param name="answerObject"></param>
        /// <returns></returns>
        public Answer FindOrCreateAnswer(AnswerType type, object answerObject)
        {
            Answer answer = new Answer(type, answerObject);
            foreach (Question question in _questions)
            {
                Answer savedAnswer = _formAnswer.FindAnswer(question);
                if (savedAnswer != null && savedAnswer.GetType() == answer.GetType() && savedAnswer.GetAnswer() == answer.GetAnswer())
                {
                    return savedAnswer;
                }
            }

            return CreateAnswer(type, answerObject);
        }
        /// <summary>
        /// Create a new Answer and add it automatically to the next unanswered question
        /// </summary>
        /// <param name="type"></param>
        /// <param name="answerObject"></param>
        /// <returns></returns>
        public Answer CreateAnswer(String type, object answerObject)
        {
            if (_activator.getAnswerType(type) == null)
            {
                throw new DllNotFoundException("Can't find type : " + type + ". Is Template missing?");
            }

            return CreateAnswer(_activator.getAnswerType(type), answerObject);
        }

        private Answer CreateAnswer(AnswerType type, object answerObject)
        {
            Answer answer = new Answer(type, answerObject);
            if (_questions.Count == 0)
            {
                throw new InvalidOperationException("Please add questions first");
            }
            Question question = FindUnansweredQuestion();
            if (question != null)
            {
                _formAnswer.AddAnswerFor(question, answer);
                return _formAnswer.FindAnswer(question);
            }
            return null;
        }

        private Question FindUnansweredQuestion()
        {
            foreach (Question question in _questions)
            {
                if (_formAnswer.FindAnswer(question) == null)
                {
                    return question;
                }
            }
            return null;
        }

        internal int GetIndex(Question question)
        {
            return _questions.IndexOf(question);
        }

        internal void setIndex(Question question, int newIndex)
        {
            _questions.RemoveAt(question.Index);
            _questions.Insert(newIndex, question);
        }

        /// <summary>
        /// Save this form into a file
        /// FileName is automatically set to Form.Title + .bin
        /// </summary>
        public void SaveForm()
        {
            Stream FileStream = File.Create(this.Title + ".bin");
            BinaryFormatter serializer = new BinaryFormatter();
            serializer.Serialize(FileStream, this);
            FileStream.Close();
        }

        //Save answers into a file
        public void SaveAnswers(String filename)
        {
            if (filename.Equals(this.Title))
            {
                throw new NotSupportedException("Form answer can't be saved into " + filename + " because this name is already reserved for the form itself");
            }
            Stream FileStream = File.Create(filename + ".bin");
            BinaryFormatter serializer = new BinaryFormatter();
            serializer.Serialize(FileStream, this._formAnswer);
            FileStream.Close();
        }

        /// <summary>
        /// Load a form from file, or create new one if fileName can't be deserialized
        /// </summary>
        /// <param name="FileName"></param>
        /// <returns></returns>
        public Form LoadForm(string FileName)
        {
            //Load Plugins in memory in order to resolve dependencies
            _activator.LoadPlugins();

            Form form = new Form();
            if (File.Exists(@FileName))
            {
                using (Stream FileStream = File.OpenRead(@FileName))
                {
                    BinaryFormatter deserializer = new BinaryFormatter();
                    form = (Form)deserializer.Deserialize(FileStream);
                    FileStream.Close();
                }
            }
            form._activator = new Activator();
            form._formAnswer = new FormAnswer();
            return form;
        }

        /// <summary>
        /// Load Answers from file or create new one if fileName can't be deserialized
        /// </summary>
        /// <param name="FileName"></param>
        public void LoadAnswers(string FileName)
        {
            //Load Plugins in memory in order to resolve dependencies
            _activator.LoadPlugins();

            FormAnswer answers = new FormAnswer();
            if (File.Exists(@FileName))
            {
                using (Stream FileStream = File.OpenRead(@FileName))
                {
                    BinaryFormatter deserializer = new BinaryFormatter();
                    answers = (FormAnswer)deserializer.Deserialize(FileStream);
                    FileStream.Close();
                }
            }

            _formAnswer = answers;
        }

    }
}
