﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TPFormulaire.Main
{
    public class DefaultAnswer : AnswerType
    {
        private string _answer;

        public override void AddAnswer(object answer)
        {
            _answer = (String)answer;
        }

        public override object GetAnswer()
        {
            return _answer;
        }
    }
}
