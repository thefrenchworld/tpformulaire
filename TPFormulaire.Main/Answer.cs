﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TPFormulaire.Main
{
    [Serializable]
    public class Answer
    {
        AnswerType _answerType;

        public Answer(AnswerType answerType, object answer)
        {
            _answerType = answerType;
            _answerType.AddAnswer(answer);
        }

        public object GetAnswer() {
            return _answerType.GetAnswer();
        }

        public void AddAnswer(object answer)
        {
           _answerType.AddAnswer(answer);
        }

        public System.Windows.Forms.Control GetControl()
        {
            return _answerType.GetControl();
        }
    }
}
