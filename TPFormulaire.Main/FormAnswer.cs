﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TPFormulaire.Main
{
    [Serializable]
    public class FormAnswer
    {
        private Dictionary<Question, Answer> _answers;

        public FormAnswer()
        {
            _answers = new Dictionary<Question, Answer>();
        }

        public IReadOnlyDictionary<Question, Answer> Answers {
            get{return _answers;}
        }

        public Answer FindAnswer(Question question)
        {
            Answer value = new Answer(new DefaultAnswer(), "");
            if (_answers.TryGetValue(question, out value))
            {
                return value;
            }
            return null;
        }

        public void AddAnswerFor(Question question, Answer answer)
        {
            _answers.Add(question, answer);
        }

        internal int CountAnswers()
        {
            return _answers.Count;
        }
    }
}
