﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TPFormulaire.Main
{
    [Serializable]
    public class Question
    {
        private QuestionType _questionType;

        public QuestionType QuestionType
        {
            get { return _questionType; }
        }
        private QuestionGroup _group;
        private Form _container;

        public QuestionGroup Group
        {
            get { return _group; }
            internal set { _group = value; }
        }

        public int Index
        {
            get { return _container.GetIndex(this); }
            set { _container.setIndex(this, value); }
        }
        
        internal Question(object title, QuestionType type, QuestionGroup group, Form container)
        {
            _questionType = type;
            _questionType.SetQuestionTitle(title);
            _group = group;
            _container = container;
        }

        public Question clone()
        {
            return new Question(_questionType.GetQuestionTitle(),_questionType.clone(),_group.clone(), _container);
        }

        public System.Windows.Forms.Control GetControl() {
           return _questionType.GetControl();
        }


    }
}
