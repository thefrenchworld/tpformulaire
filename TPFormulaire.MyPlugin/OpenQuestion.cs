﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TPFormulaire.Main
{
    [Serializable]
    public class OpenQuestion : QuestionType
    {
        public OpenQuestion()
        {

        }

        public override List<AnswerType> GetAcceptedAnswerTypes()
        {
            List<AnswerType> AcceptAnswersType = new List<AnswerType>();
            AcceptAnswersType.Add(new OpenAnswer());
            return AcceptAnswersType;
        }

        public override QuestionType clone()
        {
            return new OpenQuestion();
        }
    }
}
