﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TPFormulaire.Main
{
    [Serializable]
    public class LinkQuestion : QuestionType
    {
        private String _link;
        [NonSerialized]
        LinkLabel label;

        public String Link
        {
            get { return _link; }
            set { _link = value; }
        }
        public LinkQuestion()
        {

        }

        public LinkQuestion(String link)
        {
            _link = link;
        }

        public override List<AnswerType> GetAcceptedAnswerTypes()
        {
            List<AnswerType> AcceptAnswersType = new List<AnswerType>();
            AcceptAnswersType.Add(new OpenAnswer());
            return AcceptAnswersType;
        }

        public override QuestionType clone()
        {
            return new LinkQuestion(_link);
        }




        public override System.Windows.Forms.Control GetControl()
        {
            label = new LinkLabel();
            if (_link != null)
            {
                label.LinkArea = new LinkArea(0, label.Text.Length);
                label.Links.Add(0, label.Text.Length, _link);
                label.LinkClicked += new LinkLabelLinkClickedEventHandler(LinkedLabelClicked);
            }
            label.Text = (string)GetQuestionTitle();
            return label;
        }

        private void LinkedLabelClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            label.LinkVisited = true;
            System.Diagnostics.Process.Start(_link);
        }
    }
}
