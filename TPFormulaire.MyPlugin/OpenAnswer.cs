﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TPFormulaire.Main
{
    [Serializable]
    public class OpenAnswer : AnswerType
    {
        private string _freeAnswer;

        public string FreeAnswer
        {
            get { return _freeAnswer; }
            set { _freeAnswer = value; }
        }

        public override void AddAnswer(object answer)
        {
            _freeAnswer = (String)answer;
            base.AddAnswer(answer);
        }

        public override object GetAnswer()
        {
            return _freeAnswer;
        }

    }
}
