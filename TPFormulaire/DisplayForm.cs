﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TPFormulaire.Main;

namespace TPFormulaire
{
    public partial class DisplayForm : System.Windows.Forms.Form
    {
        private Main.Form f;
        List<ComboBox> _boxes;

        public DisplayForm()
        {
            InitializeComponent();
        }

        public DisplayForm(Main.Form f)
        {
            this.f = f;
            InitializeComponent();
            _boxes = new List<ComboBox>();
        }

        private void DisplayForm_Load(object sender, EventArgs e)
        {
            Button getAnswers = new Button();
            getAnswers.Text = "Voir les réponses";
            getAnswers.Size = new System.Drawing.Size(100, 40);
            getAnswers.Click += getAnswers_Click;

            Button addQuestion = new Button();
            addQuestion.Text = "Ajouter une question";
            addQuestion.Size = new System.Drawing.Size(100, 40);
            addQuestion.Click += addQuestion_Click;

            Button saveForm = new Button();
            saveForm.Text = "Sauvegarder le formulaire";
            saveForm.Size = new System.Drawing.Size(100, 40);
            saveForm.Click += saveForm_Click;


            if (f == null)
            {
                f = new TPFormulaire.Main.Form();
                f.Title = "Form-"+new Random().Next(0,1000);
            }
            this.Text = f.Title;
            

            TableLayoutPanel panel = new TableLayoutPanel();
            panel.Size = new System.Drawing.Size(500, 1000);
            panel.Controls.Add(saveForm);
            panel.Controls.Add(getAnswers);
            panel.Controls.Add(addQuestion);
            
            IReadOnlyList<Question> list = f.Questions;
            foreach (Question q in list)
            {
                if (q.Group != null)
                {
                    Label label = new Label();
                    label.Text = "Groupe : " + q.Group.GroupUniqueName;
                    panel.Controls.Add(label);
                }
                panel.Controls.Add(q.GetControl());

                List<AnswerType> allowedAnswersType = q.QuestionType.GetAcceptedAnswerTypes();
                ComboBox answersType = new ComboBox();
                answersType.Text = "Type de réponse à utiliser pour cette question";
                answersType.Size = new System.Drawing.Size(300, 20);
                _boxes.Add(answersType);
                if (_boxes.Count > 1)
                {
                    answersType.Enabled = false;
                }
                answersType.SelectedIndexChanged += answersType_SelectedIndexChanged;
                
                foreach (AnswerType answer in allowedAnswersType)
                {
                    answersType.Items.Add(answer.GetType());
                }
                panel.Controls.Add(answersType);
            }
            this.Controls.Add(panel);
        }

        private void saveForm_Click(object sender, EventArgs e)
        {
            f.SaveForm();
            MessageBox.Show("Formulaire sauvegardé dans " + System.IO.Directory.GetCurrentDirectory() + "/" + f.Title + ".bin");
        }

        private void addQuestion_Click(object sender, EventArgs e)
        {
            new QuestionForm(f).Show();
            this.Close();
        }

        private void answersType_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox combo = (ComboBox)sender;
            combo.Enabled = false;
            if (_boxes.IndexOf(combo) + 1 < _boxes.Count)
            {
                _boxes[_boxes.IndexOf(combo) + 1].Enabled = true;
            }
            new AnswerForm(f.FindOrCreateAnswer("TPFormulaire.Main.OpenAnswer", "Votre réponse ici")).Show();

        }

        private void getAnswers_Click(object sender, EventArgs e)
        {
            new ShowAnswers(f).Show();
        }
    }
}
