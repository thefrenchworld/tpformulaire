﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TPFormulaire
{
    public partial class QuestionForm : Form
    {
        private Main.Form f;
        private IReadOnlyList<Type> types;
        TableLayoutPanel panel;
        Control createControl;
        TPFormulaire.Main.QuestionType question;

        public QuestionForm()
        {
            InitializeComponent();
        }

        public QuestionForm(Main.Form f)
        {
            this.f = f;
            InitializeComponent();
        }

        private void QuestionForm_Load(object sender, EventArgs e)
        {

            types = f.Types;

            panel = new TableLayoutPanel();
            panel.Size = new System.Drawing.Size(500, 300);
            Label yourQuestion = new Label();
            yourQuestion.Text = "Choisissez le type de question à ajouter";
            yourQuestion.Size = new System.Drawing.Size(200, 40);

            ComboBox comboTypes = new ComboBox();
            comboTypes.Text = "Types de question";
            comboTypes.Size = new System.Drawing.Size(300, 20);
            comboTypes.SelectedIndexChanged += comboTypes_SelectedIndexChanged;

            foreach (Type type in types)
            {
                if (type.BaseType.FullName.Equals("TPFormulaire.Main.QuestionType"))
                {
                    comboTypes.Items.Add(type.FullName);
                }
            }
            panel.Controls.Add(comboTypes);

            this.Controls.Add(panel);
        }

        void comboTypes_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox box = (ComboBox)sender;
            box.Enabled = false;
            Type selectedType = types.First(x => x.FullName.Equals(box.Text));
            question = System.Activator.CreateInstance(selectedType) as TPFormulaire.Main.QuestionType;
            createControl = question.GetCreateControl();
            panel.Controls.Add(createControl);

            Button submit = new Button();
            submit.Text = "Sauvegarder";
            submit.Size = new System.Drawing.Size(200, 40);
            submit.Click += submit_Click;
            panel.Controls.Add(submit);
        }

        private void submit_Click(object sender, EventArgs e)
        {
            f.CreateQuestion(question, createControl.Text);
            new DisplayForm(f).Show();
            this.Close();
        }
    }
}
