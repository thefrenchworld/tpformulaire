﻿namespace TPFormulaire
{
    partial class MainForm
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.LoadForm = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(64, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(157, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Sélection du Formulaire";
            // 
            // LoadForm
            // 
            this.LoadForm.Location = new System.Drawing.Point(30, 44);
            this.LoadForm.Name = "LoadForm";
            this.LoadForm.Size = new System.Drawing.Size(240, 47);
            this.LoadForm.TabIndex = 1;
            this.LoadForm.Text = "Charger un formulaire existant";
            this.LoadForm.UseVisualStyleBackColor = true;
            this.LoadForm.Click += new System.EventHandler(this.LoadForm_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(30, 125);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(240, 47);
            this.button2.TabIndex = 2;
            this.button2.Text = "Nouveau Formulaire";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(282, 253);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.LoadForm);
            this.Controls.Add(this.label1);
            this.Name = "MainForm";
            this.Text = "Selection";
            this.Load += new System.EventHandler(this.DisplayForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button LoadForm;
        private System.Windows.Forms.Button button2;
    }
}

