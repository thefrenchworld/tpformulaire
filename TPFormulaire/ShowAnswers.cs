﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TPFormulaire
{
    public partial class ShowAnswers : Form
    {
        private Main.Form f;


        public ShowAnswers()
        {
            InitializeComponent();
        }

        public ShowAnswers(Main.Form f)
        {
            this.f = f;
            InitializeComponent();
        }




        private void ShowAnswers_Load(object sender, EventArgs e)
        {
            TableLayoutPanel panel = new TableLayoutPanel();
            panel.Size = new System.Drawing.Size(500, 1000);

            foreach (KeyValuePair<Main.Question, Main.Answer> a in f.Answers)
            {
                panel.Controls.Add(a.Key.GetControl());
                Control answerControl = a.Value.GetControl();
                answerControl.Enabled = false;
                panel.Controls.Add(answerControl);
            }


            Button load = new Button();
            load.Text = "Charger les réponses";
            load.Click += load_Click;
            load.Size = new System.Drawing.Size(100, 40);
            panel.Controls.Add(load);

            Button save = new Button();
            save.Text = "Sauvegarder les réponses";
            save.Click += save_Click;
            save.Size = new System.Drawing.Size(100, 40);
            panel.Controls.Add(save);

            this.Controls.Add(panel);
        }

        private void load_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            openFileDialog1.InitialDirectory = System.Environment.CurrentDirectory;
            openFileDialog1.Filter = "bin files (*.bin)|*.bin";
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                f.LoadAnswers(openFileDialog1.FileName);
                new ShowAnswers(f).Show();
                this.Close();
            }
        }

        private void save_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();

            saveFileDialog1.Filter = "bin files (*.bin)|*.txt";
            saveFileDialog1.FilterIndex = 2;
            saveFileDialog1.RestoreDirectory = true;

            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                f.SaveAnswers(saveFileDialog1.FileName);
                MessageBox.Show("Réponses sauvegardées dans " +saveFileDialog1.FileName);
            }
            
        }
    }
}
