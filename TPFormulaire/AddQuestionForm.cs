﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TPFormulaire
{
    public partial class AddAnswerForm : Form
    {
        private Main.Answer answer;

        public AddAnswerForm()
        {
            InitializeComponent();
        }

        public AddAnswerForm(Main.Answer answer)
        {
            this.answer = answer;
            InitializeComponent();

            
        }

        private void AnswerForm_Load(object sender, EventArgs e)
        {
            TableLayoutPanel panel = new TableLayoutPanel();
            panel.Size = new System.Drawing.Size(500, 300);
            Label yourAnswer = new Label();
            yourAnswer.Text = "Entrez votre réponse ci-dessous";
            yourAnswer.Size = new System.Drawing.Size(200, 40);
            panel.Controls.Add(yourAnswer);
            panel.Controls.Add(answer.GetControl());

            Button submit = new Button();
            submit.Text = "Sauvegarder";
            submit.Size = new System.Drawing.Size(200, 40);
            submit.Click += submit_Click;
            panel.Controls.Add(submit);

            this.Controls.Add(panel);
        }

        private void submit_Click(object sender, EventArgs e)
        {
            answer.AddAnswer(answer.GetControl().Text);
            this.Close();
        }

        
    }
}
